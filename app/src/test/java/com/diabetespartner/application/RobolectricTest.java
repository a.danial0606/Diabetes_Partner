package com.diabetespartner.application;

import com.diabetespartner.application.analytics.Analytics;
import com.diabetespartner.application.db.DatabaseHandler;
import com.diabetespartner.application.presenter.HelloPresenter;
import com.diabetespartner.application.tools.LocaleHelper;
import com.diabetespartner.application.backup.Backup;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.RobolectricTestRunner;

@Ignore
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public abstract class RobolectricTest {
    protected Analytics getAnalytics() {
        return getTestApplication().getAnalytics();
    }

    protected Backup getBackup() {
        return getTestApplication().getBackup();
    }

    private TestDiabetesPartner getTestApplication() {
        return (TestDiabetesPartner) RuntimeEnvironment.application;
    }

    protected DatabaseHandler getDBHandler() {
        return getTestApplication().getDBHandler();
    }

    protected HelloPresenter getHelloPresenter() {
        //noinspection ConstantConditions
        return getTestApplication().createHelloPresenter(null);
    }

    protected LocaleHelper getLocaleHelper() {
        //noinspection ConstantConditions
        return getTestApplication().getLocaleHelper();
    }
}
