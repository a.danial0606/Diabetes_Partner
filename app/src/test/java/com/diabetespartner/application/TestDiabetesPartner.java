package com.diabetespartner.application;

import android.support.annotation.NonNull;

import com.diabetespartner.application.activity.A1cCalculatorActivity;
import com.diabetespartner.application.activity.HelloActivity;
import com.diabetespartner.application.analytics.Analytics;
import com.diabetespartner.application.backup.Backup;
import com.diabetespartner.application.db.DatabaseHandler;
import com.diabetespartner.application.presenter.A1CCalculatorPresenter;
import com.diabetespartner.application.presenter.HelloPresenter;
import com.diabetespartner.application.tools.LocaleHelper;

import org.mockito.Mock;

import static org.mockito.MockitoAnnotations.initMocks;

public class TestDiabetesPartner extends DiabetesPartner {
    @Mock
    private Backup backupMock;

    @Mock
    private Analytics analyticsMock;

    @Mock
    private DatabaseHandler dbHandlerMock;

    @Mock
    private A1CCalculatorPresenter a1CCalculatorPresenterMock;

    @Mock
    private HelloPresenter helloPresenterMock;

    @Mock
    private LocaleHelper localeHelperMock;

    @Override
    public void onCreate() {
        super.onCreate();

        initMocks(this);
    }

    @NonNull
    @Override
    public Backup getBackup() {
        return backupMock;
    }

    @NonNull
    @Override
    public Analytics getAnalytics() {
        return analyticsMock;
    }

    @NonNull
    @Override
    public DatabaseHandler getDBHandler() {
        return dbHandlerMock;
    }

    @NonNull
    @Override
    public A1CCalculatorPresenter createA1cCalculatorPresenter(@NonNull A1cCalculatorActivity activity) {
        return a1CCalculatorPresenterMock;
    }

    @Override
    protected void initLanguage() {
        //nothing
    }

    @NonNull
    @Override
    public HelloPresenter createHelloPresenter(@NonNull final HelloActivity activity) {
        return helloPresenterMock;
    }

    @NonNull
    @Override
    public LocaleHelper getLocaleHelper() {
        return localeHelperMock;
    }
}
