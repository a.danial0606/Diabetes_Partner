package com.diabetespartner.application.tools.network;

public class Links {
  public static final String TERMS = "http://www.diabetespartner.com/terms/";
  public static final String LICENSES = "http://www.diabetespartner.com/third-party-licenses/";
  public static final String PRIVACY = "http://www.diabetespartner.comprivacy/";
  public static final String THANKS = "http://www.diabetespartner.com/";
}