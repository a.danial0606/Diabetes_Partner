package com.diabetespartner.application.tools;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;

import com.diabetespartner.application.db.DatabaseHandler;
import com.diabetespartner.application.db.Reminder;
import com.diabetespartner.application.receivers.BroadcastReceiver;

import org.joda.time.DateTime;

import java.util.Calendar;
import java.util.List;

public class DiabetesPartnerAlarmManager {
    private Context context;
    private android.app.AlarmManager alarmMgr;
    private DatabaseHandler db;

    public DiabetesPartnerAlarmManager(Context context) {
        this.context = context;
        this.db = new DatabaseHandler(context);
        this.alarmMgr = (android.app.AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    }

    public void setAlarms() {
        List<Reminder> reminders = db.getReminders();
        int activeRemindersCount = 0;

        // Set an alarm for each date
        for (int i = 0; i < reminders.size(); i++) {
            Reminder reminder = reminders.get(i);

            Intent intent = new Intent(context, BroadcastReceiver.class);
            intent.putExtra("metric", reminder.getMetric());
            intent.putExtra("glucosio_reminder", true);
            intent.putExtra("reminder_label", reminder.getLabel());

            PendingIntent alarmIntent = PendingIntent.getBroadcast(context, (int) reminder.getId(), intent, 0);

            if (reminder.isActive()) {
                activeRemindersCount++;
                Calendar calNow = Calendar.getInstance();
                Calendar calAlarm = Calendar.getInstance();
                calAlarm.setTime(reminder.getAlarmTime());
                calAlarm.set(Calendar.SECOND, 0);

                DateTime now = new DateTime(calNow.getTime());
                DateTime reminderDate = new DateTime(calAlarm);

                if (reminderDate.isBefore(now)) {
                    calAlarm.set(Calendar.DATE, calNow.get(Calendar.DATE));
                    calAlarm.add(Calendar.DATE, 1);
                }

                Log.d("DiabetesPartner", "Added reminder on " + calAlarm.get(Calendar.DAY_OF_MONTH) + " at " + calAlarm.get(Calendar.HOUR) + ":" + calAlarm.get(Calendar.MINUTE));

                alarmMgr.setRepeating(android.app.AlarmManager.RTC_WAKEUP, calAlarm.getTimeInMillis(),
                        android.app.AlarmManager.INTERVAL_DAY, alarmIntent);
            } else {
                alarmMgr.cancel(alarmIntent);
            }
        }

        enableBootReceiver(activeRemindersCount > 0);
    }

    private void enableBootReceiver(boolean value) {
        ComponentName receiver = new ComponentName(context, BroadcastReceiver.class);
        PackageManager pm = context.getPackageManager();

        int componentState = value ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED : PackageManager.COMPONENT_ENABLED_STATE_DISABLED;

        pm.setComponentEnabledSetting(receiver,
                componentState,
                PackageManager.DONT_KILL_APP);
    }
}
