package com.diabetespartner.application.tools.network;

public interface NetworkConnectivity {
  boolean isConnected();
}
