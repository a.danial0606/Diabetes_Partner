package com.diabetespartner.application.presenter;

import android.util.Log;

import com.diabetespartner.application.activity.MainActivity;
import com.diabetespartner.application.db.DatabaseHandler;

public class MainPresenter {

    private DatabaseHandler dB;

    public MainPresenter(MainActivity mainActivity, DatabaseHandler databaseHandler) {
        dB = databaseHandler;
        Log.i("msg::", "initiated dB object");
        if (dB.getUser(1) == null) {
            // if user doesn't exists start hello activity
            mainActivity.startHelloActivity();
        }
    }

    public boolean isdbEmpty() {
        return dB.getGlucoseReadings().size() == 0;
    }
}
