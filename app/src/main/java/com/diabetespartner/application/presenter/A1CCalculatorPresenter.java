package com.diabetespartner.application.presenter;

import android.support.annotation.NonNull;

import com.diabetespartner.application.activity.A1cCalculatorActivity;
import com.diabetespartner.application.db.DatabaseHandler;
import com.diabetespartner.application.db.HB1ACReading;
import com.diabetespartner.application.db.User;
import com.diabetespartner.application.tools.DataConverter;

import java.util.Date;

public class A1CCalculatorPresenter {
    private final DatabaseHandler dbHandler;
    private final A1cCalculatorActivity activity;

    public A1CCalculatorPresenter(@NonNull final A1cCalculatorActivity activity,
                                  @NonNull final DatabaseHandler dbHandler) {
        this.activity = activity;
        this.dbHandler = dbHandler;
    }

    public double calculateA1C(String glucose) {
        if (isInvalidDouble(glucose)) {
            return 0;
        }

        double convertedA1C;
        User user = dbHandler.getUser(1);

        if ("mg/dL".equals(user.getPreferred_unit())) {
            convertedA1C = DataConverter.glucoseToA1C(Double.parseDouble(glucose));
        } else {
            convertedA1C = DataConverter.glucoseToA1C(DataConverter.glucoseToMgDl(Double.parseDouble(glucose)));
        }
        if ("percentage".equals(user.getPreferred_unit_a1c())) {
            return convertedA1C;
        } else {
            return DataConverter.a1cNgspToIfcc(convertedA1C);
        }
    }

    private boolean isInvalidDouble(String value) {
        return value == null || value.length() == 0 || (value.length() == 1 && !Character.isDigit(value.charAt(0)));
    }

    public void checkGlucoseUnit() {
        if (!dbHandler.getUser(1).getPreferred_unit().equals("mg/dL")) {
            activity.setMmol();
        }
    }

    public void saveA1C(double a1c) {
        User user = dbHandler.getUser(1);
        double finalA1c = a1c;
        if (!"percentage".equals(user.getPreferred_unit_a1c())) {
            finalA1c = DataConverter.a1cIfccToNgsp(a1c);
        }

        HB1ACReading a1cReading = new HB1ACReading(finalA1c, new Date());
        dbHandler.addHB1ACReading(a1cReading);
        activity.finish();
    }

    public String getA1cUnit() {
        return dbHandler.getUser(1).getPreferred_unit_a1c();
    }

}
