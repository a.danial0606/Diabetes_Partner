package com.diabetespartner.application.presenter;

import com.diabetespartner.application.db.DatabaseHandler;
import com.diabetespartner.application.fragment.HistoryFragment;

import java.util.ArrayList;

public class HistoryPresenter {

    private DatabaseHandler dB;
    private HistoryFragment fragment;

    public HistoryPresenter(HistoryFragment historyFragment) {
        this.fragment = historyFragment;
        dB = new DatabaseHandler(historyFragment.getContext());
    }

    public boolean isdbEmpty() {
        return dB.getGlucoseReadings().size() == 0;
    }


    public String convertDate(String date) {
        return fragment.convertDate(date);
    }

    public void onDeleteClicked(long idToDelete, int metricID) {
        switch (metricID) {
            // Glucose
            case 0:
                dB.deleteGlucoseReading(dB.getGlucoseReadingById(idToDelete));
                fragment.reloadFragmentAdapter();
                break;
            // HB1AC
            case 1:
                dB.deleteHB1ACReading(dB.getHB1ACReadingById(idToDelete));
                fragment.reloadFragmentAdapter();
                break;
            // Weight
            case 2:
                dB.deleteWeightReading(dB.getWeightReadingById(idToDelete));
                fragment.reloadFragmentAdapter();
                break;
            default:
                break;
        }
        fragment.notifyAdapter();
        fragment.updateToolbarBehaviour();
    }

    // Getters
    public String getUnitMeasuerement() {
        return dB.getUser(1).getPreferred_unit();
    }

    public String getWeightUnitMeasurement() {
        return dB.getUser(1).getPreferred_unit_weight();
    }

    public String getA1cUnitMeasurement() {
        return dB.getUser(1).getPreferred_unit_a1c();
    }

    public ArrayList<Long> getGlucoseId() {
        return dB.getGlucoseIdAsArray();
    }

    public ArrayList<String> getGlucoseReadingType() {
        return dB.getGlucoseTypeAsArray();
    }

    public ArrayList<String> getGlucoseNotes() {
        return dB.getGlucoseNotesAsArray();
    }

    public ArrayList<Integer> getGlucoseReading() {
        return dB.getGlucoseReadingAsArray();
    }

    public ArrayList<String> getGlucoseDateTime() {
        return dB.getGlucoseDateTimeAsArray();
    }

    public int getGlucoseReadingsNumber() {
        return dB.getGlucoseReadingAsArray().size();
    }


    public ArrayList<Long> getHB1ACId() {
        return dB.getHB1ACIdAsArray();
    }

    public ArrayList<String> getHB1ACDateTime() {
        return dB.getHB1ACDateTimeAsArray();
    }

    public ArrayList<Double> getHB1ACReading() {
        return dB.getHB1ACReadingAsArray();
    }

    public int getHB1ACReadingsNumber() {
        return dB.getHB1ACReadingAsArray().size();
    }

    public ArrayList<Integer> getWeightReadings() {
        return dB.getWeightReadingAsArray();
    }

    public ArrayList<String> getWeightDateTime() {
        return dB.getWeightReadingDateTimeAsArray();
    }

    public ArrayList<Long> getWeightId() {
        return dB.getWeightIdAsArray();
    }

    public int getWeightReadingsNumber() {
        return dB.getWeightIdAsArray().size();
    }
}
