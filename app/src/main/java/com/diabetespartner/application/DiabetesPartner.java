package com.diabetespartner.application;

import android.app.Application;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import com.diabetespartner.application.activity.A1cCalculatorActivity;
import com.diabetespartner.application.analytics.Analytics;
import com.diabetespartner.application.analytics.GoogleAnalytics;
import com.diabetespartner.application.backup.GoogleDriveBackup;
import com.diabetespartner.application.db.DatabaseHandler;
import com.diabetespartner.application.tools.LocaleHelper;
import com.diabetespartner.application.tools.Preferences;
import com.diabetespartner.application.activity.HelloActivity;
import com.diabetespartner.application.backup.Backup;
import com.diabetespartner.application.db.User;
import com.diabetespartner.application.presenter.A1CCalculatorPresenter;
import com.diabetespartner.application.presenter.HelloPresenter;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class DiabetesPartner extends Application {

    private static DiabetesPartner sInstance;

    @Nullable
    private Analytics analytics;

    @Nullable
    private LocaleHelper localeHelper;

    @Nullable
    private Preferences preferences;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        //initFont();
        initLanguage();
    }

/*    @VisibleForTesting
    protected void initFont() {
        //TODO: convert of using new introduced class Preferences
        // Get Dyslexia preference and adjust font
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isDyslexicModeOn = sharedPref.getBoolean("pref_font_dyslexia", false);

        if (isDyslexicModeOn) {
            setFont("fonts/opendyslexic.otf");
        } else {
            setFont("fonts/lato.ttf");
        }
    }*/

    @VisibleForTesting
    protected void initLanguage() {
        User user = getDBHandler().getUser(1);
        if (user != null) {
            checkBadLocale(user);

           /* String languageTag = user.getPreferred_language();
            if (languageTag != null) {
                getLocaleHelper().updateLanguage(this, languageTag);
            }*/
        }
    }

    private void checkBadLocale(User user) {
        Preferences preferences = getPreferences();
        boolean cleanLocaleDone = preferences.isLocaleCleaned();

        if (!cleanLocaleDone) {
            User updatedUser = new User(user.getId(), user.getName(),
                    user.getCountry(), user.getAge(), user.getGender(), user.getD_type(),
                    user.getPreferred_unit(), user.getPreferred_unit_a1c(),
                    user.getPreferred_unit_weight(), user.getPreferred_range(),
                    user.getCustom_range_min(), user.getCustom_range_max());
            getDBHandler().updateUser(updatedUser);
            preferences.saveLocaleCleaned();
        }
    }

    private void setFont(String font) {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(font)
                .setFontAttrId(R.attr.fontPath)
                .build());
    }

    @NonNull
    public Backup getBackup() {
        return new GoogleDriveBackup();
    }

    @NonNull
    public Analytics getAnalytics() {
        if (analytics == null) {
            analytics = new GoogleAnalytics();
            analytics.init(this);
        }

        return analytics;
    }

    @NonNull
    public DatabaseHandler getDBHandler() {
        return new DatabaseHandler(getApplicationContext());
    }

    @NonNull
    public A1CCalculatorPresenter createA1cCalculatorPresenter(@NonNull final A1cCalculatorActivity activity) {
        return new A1CCalculatorPresenter(activity, getDBHandler());
    }

    @NonNull
    public LocaleHelper getLocaleHelper() {
        if (localeHelper == null) {
            localeHelper = new LocaleHelper();
        }
        return localeHelper;
    }

    @NonNull
    public Preferences getPreferences() {
        if (preferences == null) {
            preferences = new Preferences(this);
        }

        return preferences;
    }

    public static DiabetesPartner getInstance() {
        if (sInstance == null) {
            sInstance = new DiabetesPartner();
        }
        return sInstance;
    }

    @NonNull
    public HelloPresenter createHelloPresenter(@NonNull final HelloActivity activity) {
        return new HelloPresenter(activity, getDBHandler());
    }
}