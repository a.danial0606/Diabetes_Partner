package com.diabetespartner.application.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.diabetespartner.application.db.HB1ACReading;
import com.diabetespartner.application.tools.FormatDateTime;
import com.diabetespartner.application.presenter.AddA1CPresenter;

import java.util.Calendar;

public class AddA1CActivity extends AddReadingActivity {

    private TextView readingTextView;
    private TextView unitTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.diabetespartner.application.R.layout.activity_add_hb1ac);
        Toolbar toolbar = (Toolbar) findViewById(com.diabetespartner.application.R.id.activity_main_toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setElevation(2);
        }

        this.retrieveExtra();

        AddA1CPresenter presenter = new AddA1CPresenter(this);
        setPresenter(presenter);
        presenter.setReadingTimeNow();

        readingTextView = (TextView) findViewById(com.diabetespartner.application.R.id.hb1ac_add_value);
        unitTextView = (TextView) findViewById(com.diabetespartner.application.R.id.hb1ac_unit);

        this.createDateTimeViewAndListener();
        this.createFANViewAndListener();

        if (!"percentage".equals(presenter.getA1CUnitMeasuerement())) {
            unitTextView.setText(getString(com.diabetespartner.application.R.string.mmol_mol));
        }

        // If an id is passed, open the activity in edit mode
        FormatDateTime formatDateTime = new FormatDateTime(getApplicationContext());
        if (this.isEditing()) {
            setTitle(com.diabetespartner.application.R.string.title_activity_add_hb1ac_edit);
            HB1ACReading readingToEdit = presenter.getHB1ACReadingById(getEditId());
            readingTextView.setText(readingToEdit.getReading() + "");
            Calendar cal = Calendar.getInstance();
            cal.setTime(readingToEdit.getCreated());
            this.getAddDateTextView().setText(formatDateTime.getDate(cal));
            this.getAddTimeTextView().setText(formatDateTime.getTime(cal));
            presenter.updateReadingSplitDateTime(readingToEdit.getCreated());
        } else {
            this.getAddDateTextView().setText(formatDateTime.getCurrentDate());
            this.getAddTimeTextView().setText(formatDateTime.getCurrentTime());
        }

    }

    @Override
    protected void dialogOnAddButtonPressed() {
        AddA1CPresenter presenter = (AddA1CPresenter) getPresenter();
        if (this.isEditing()) {
            presenter.dialogOnAddButtonPressed(this.getAddTimeTextView().getText().toString(),
                    this.getAddDateTextView().getText().toString(), readingTextView.getText().toString(), this.getEditId());
        } else {
            presenter.dialogOnAddButtonPressed(this.getAddTimeTextView().getText().toString(),
                    this.getAddDateTextView().getText().toString(), readingTextView.getText().toString());
        }
    }

    public void showErrorMessage() {
        Toast.makeText(getApplicationContext(), getString(com.diabetespartner.application.R.string.dialog_error2), Toast.LENGTH_SHORT).show();
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }
}
