package com.diabetespartner.application.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.diabetespartner.application.DiabetesPartner;
import com.diabetespartner.application.analytics.Analytics;
import com.diabetespartner.application.tools.LabelledSpinner;
import com.diabetespartner.application.tools.LocaleHelper;
import com.diabetespartner.application.view.HelloView;
import com.diabetespartner.application.presenter.HelloPresenter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HelloActivity extends AppCompatActivity implements HelloView {

    @BindView(com.diabetespartner.application.R.id.activity_name)
    TextView nameTextView;

    @BindView(com.diabetespartner.application.R.id.activity_hello_spinner_country)
    LabelledSpinner countrySpinner;

    @BindView(com.diabetespartner.application.R.id.activity_hello_spinner_language)
    LabelledSpinner languageSpinner;

    @BindView(com.diabetespartner.application.R.id.activity_hello_spinner_gender)
    LabelledSpinner genderSpinner;

    @BindView(com.diabetespartner.application.R.id.activity_hello_spinner_diabetes_type)
    LabelledSpinner typeSpinner;

    @BindView(com.diabetespartner.application.R.id.activity_hello_spinner_preferred_unit)
    LabelledSpinner unitSpinner;

    @BindView(com.diabetespartner.application.R.id.activity_hello_button_start)
    Button startButton;

    @BindView(com.diabetespartner.application.R.id.activity_hello_age)
    TextView ageTextView;

    private HelloPresenter presenter;

    private List<String> localesWithTranslation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.diabetespartner.application.R.layout.activity_hello);

        ButterKnife.bind(this);

        // Prevent SoftKeyboard to pop up on start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        DiabetesPartner application = (DiabetesPartner) getApplication();
        presenter = application.createHelloPresenter(this);
        presenter.loadDatabase();

        final LocaleHelper localeHelper = application.getLocaleHelper();
        initCountrySpinner(localeHelper);
        initLanguageSpinner(localeHelper);

        genderSpinner.setItemsArray(com.diabetespartner.application.R.array.helloactivity_gender_list);
        unitSpinner.setItemsArray(com.diabetespartner.application.R.array.helloactivity_preferred_glucose_unit);
        typeSpinner.setItemsArray(com.diabetespartner.application.R.array.helloactivity_diabetes_type);

        initStartButton();

        Analytics analytics = application.getAnalytics();
        analytics.reportScreen("Hello Activity");
        Log.i("HelloActivity", "Setting screen name: hello");
    }

    private void initLanguageSpinner(final LocaleHelper localeHelper) {
        localesWithTranslation = localeHelper.getLocalesWithTranslation(getResources());

        List<String> displayLanguages = new ArrayList<>(localesWithTranslation.size());
        for (String language : localesWithTranslation) {
            if (language.length() > 0) {
                displayLanguages.add(localeHelper.getDisplayLanguage(language));
            }
        }

        languageSpinner.setItemsArray(displayLanguages);

        final Locale deviceLocale = localeHelper.getDeviceLocale();
        String displayLanguage = localeHelper.getDisplayLanguage(deviceLocale.toString());

        setSelection(displayLanguage, languageSpinner);
    }

    private void setSelection(final String label, final LabelledSpinner labelledSpinner) {
        if (label != null) {
            int position = ((ArrayAdapter) labelledSpinner.getSpinner().getAdapter()).getPosition(label);
            labelledSpinner.setSelection(position);
        }
    }

    private void initStartButton() {
        final Drawable pinkArrow = ResourcesCompat.getDrawable(getResources(),
                com.diabetespartner.application.R.drawable.ic_navigate_next_pink_24px, null);
        if (pinkArrow != null) {
            pinkArrow.setBounds(0, 0, 60, 60);
            startButton.setCompoundDrawables(null, null, pinkArrow, null);
        }
    }

    private void initCountrySpinner(final LocaleHelper localeHelper) {
        // Get countries list from locale
        ArrayList<String> countries = new ArrayList<>();
        Locale[] locales = Locale.getAvailableLocales();

        for (Locale locale : locales) {
            String country = locale.getDisplayCountry();

            if ((country.trim().length() > 0) && (!countries.contains(country))) {
                countries.add(country);
            }
        }

        Collections.sort(countries);

        // Populate Spinners with array
        countrySpinner.setItemsArray(countries);

        // Get locale country name and set the spinner
        String localCountry = localeHelper.getDeviceLocale().getDisplayCountry();

        setSelection(localCountry, countrySpinner);
    }

    @OnClick(com.diabetespartner.application.R.id.activity_hello_button_start)
    void onStartClicked() {
        presenter.onNextClicked(nameTextView.getText().toString(),ageTextView.getText().toString(),
                genderSpinner.getSpinner().getSelectedItem().toString(),
                localesWithTranslation.get(languageSpinner.getSpinner().getSelectedItemPosition()),
                countrySpinner.getSpinner().getSelectedItem().toString(),
                typeSpinner.getSpinner().getSelectedItemPosition() + 1,
                unitSpinner.getSpinner().getSelectedItem().toString());
    }

    public void displayErrorWrongAge() {
        //Why toast and not error in edit box or dialog
        Toast.makeText(getApplicationContext(), getString(com.diabetespartner.application.R.string.helloactivity_age_invalid), Toast.LENGTH_SHORT).show();
    }

    public void startMainView() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
