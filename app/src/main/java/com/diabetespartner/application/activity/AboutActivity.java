package com.diabetespartner.application.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.diabetespartner.application.DiabetesPartner;
import com.diabetespartner.application.R;
import com.diabetespartner.application.analytics.Analytics;
import com.diabetespartner.application.tools.network.Links;

import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.diabetespartner.application.R.layout.preferences_about);

        getFragmentManager().beginTransaction()
                .replace(com.diabetespartner.application.R.id.aboutPreferencesFrame, new MyPreferenceFragment()).commit();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.about_DP));
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public static class MyPreferenceFragment extends PreferenceFragment {

        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(com.diabetespartner.application.R.xml.about_preference);

            final Preference licencesPref = (Preference) findPreference("preference_licences");
            final Preference ratePref = (Preference) findPreference("rate_DP");
            final Preference feedbackPref = (Preference) findPreference("preference_feedback");
            final Preference privacyPref = (Preference) findPreference("preference_privacy");
            final Preference termsPref = (Preference) findPreference("preference_terms");
            final Preference versionPref = (Preference) findPreference("preference_version");
            final Preference thanksPref = (Preference) findPreference("preference_thanks");


            termsPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    ExternalLinkActivity.launch(
                        getActivity(),
                        getString(com.diabetespartner.application.R.string.preferences_terms),
                        Links.TERMS);
                    addTermsAnalyticsEvent("Diabetes Partner Terms opened");
                    return false;
                }
            });

            licencesPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    ExternalLinkActivity.launch(
                        getActivity(),
                        getString(com.diabetespartner.application.R.string.preferences_licences_open),
                        Links.LICENSES);
                    addTermsAnalyticsEvent("Diabetes Partner Licence opened");
                    return false;
                }
            });

            ratePref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.diabetespartner.application"));
                    startActivity(intent);

                    return false;
                }
            });

            feedbackPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    // Open email intent
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:hello@DiabetesPartner.com"));
                    boolean activityExists = emailIntent.resolveActivityInfo(getActivity().getPackageManager(), 0) != null;

                    if (activityExists) {
                        startActivity(emailIntent);
                    } else {
                        Toast.makeText(getActivity().getApplicationContext(), getResources().getString(com.diabetespartner.application.R.string.menu_support_error1), Toast.LENGTH_LONG).show();
                    }

                    return false;
                }
            });

            privacyPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    ExternalLinkActivity.launch(
                        getActivity(),
                        getString(com.diabetespartner.application.R.string.preferences_privacy),
                        Links.PRIVACY);
                    addTermsAnalyticsEvent("Diabetes Partner Privacy opened");
                    return false;
                }
            });

            thanksPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    ExternalLinkActivity.launch(
                            getActivity(),
                            getString(com.diabetespartner.application.R.string.preferences_contributors),
                            Links.THANKS);
                    addTermsAnalyticsEvent("Diabetes Partner Contributors opened");
                    return false;
                }
            });

            versionPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

                int easterEggCount;

                @Override
                public boolean onPreferenceClick(Preference preference) {
                    if (easterEggCount == 6) {
                        String uri = String.format(Locale.ENGLISH, "geo:%f,%f", 40.794010, 17.124583);
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        getActivity().startActivity(intent);
                        easterEggCount = 0;
                    } else {
                        this.easterEggCount = easterEggCount + 1;
                    }
                    return false;
                }
            });

        }

        private void addTermsAnalyticsEvent(String action) {
            Analytics analytics = ((DiabetesPartner) getActivity().getApplication()).getAnalytics();

            analytics.reportAction("Preferences", action);
        }
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }
}
