package com.diabetespartner.application.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.diabetespartner.application.BuildConfig;
import com.diabetespartner.application.R;
import com.diabetespartner.application.object.Restaurant;
import com.diabetespartner.application.tools.AnimationTools;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;


/**
 * Created by ADANI on 11/14/2017.
 */

public class AddRestaurantActivity extends MainActivity{
    private static final String TAG = "DiabetesPartner";
    private TextView addLatitude;
    private TextView addLongitude;
    private EditText restaurantName;
    private EditText menu;
    private LocationManager mLocationManager = null;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private FusedLocationProviderClient mFusedLocationClient;
    private FloatingActionButton doneFAB;
    private Runnable fabAnimationRunnable;
    private final java.lang.String INTENT_EXTRA_EDIT = "editing";
    private final java.lang.String INTENT_EXTRA_EDIT_ID = "edit_id";
    private final String INTENT_EXTRA_PAGER = "pager";
    private final String INTENT_EXTRA_DROPDOWN = "history_dropdown";


    private DatabaseReference ref;
    Location mLastLocation;
    private int pagerPosition;
    private int dropdownPosition;
    private long editId = 0;
    private boolean editing = false;


    private Restaurant restaurant = new Restaurant();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_restaurant);
        Toolbar toolbar = (Toolbar) findViewById(com.diabetespartner.application.R.id.activity_main_toolbar);

        retrieveExtra();


        ref =  FirebaseDatabase.getInstance().getReferenceFromUrl("https://fir-diabetespartner.firebaseio.com");

        /*FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference();*/

        addLatitude = (TextView) findViewById(R.id.add_latitude);
        addLongitude  = (TextView) findViewById(R.id.add_longitude);
        restaurantName = (EditText) findViewById(R.id.add_restaurant_name);
        menu  = (EditText) findViewById(R.id.add_menu);


        createFANViewAndListener();


        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setElevation(2);
        }

    }

    public void showErrorMessage() {
        View rootLayout = findViewById(android.R.id.content);
        Snackbar.make(rootLayout, getString(com.diabetespartner.application.R.string.dialog_error2), Snackbar.LENGTH_SHORT).show();
    }


    public void showDuplicateErrorMessage() {
        View rootLayout = findViewById(android.R.id.content);
        Snackbar.make(rootLayout, getString(com.diabetespartner.application.R.string.dialog_error_duplicate), Snackbar.LENGTH_SHORT).show();
    }

    protected void retrieveExtra() {
        Bundle b = getIntent().getExtras();
        if (b != null) {
            pagerPosition = b.getInt(INTENT_EXTRA_PAGER);
            editId = b.getLong(INTENT_EXTRA_EDIT_ID);
            editing = b.getBoolean(INTENT_EXTRA_EDIT);
            dropdownPosition = b.getInt(INTENT_EXTRA_DROPDOWN);
        }
    }


    /*public void getRestaurantDetails() {
        String restaurant1 = restaurantName.getText().toString();
        String menu1 = menu.getText().toString();

        if(TextUtils.isEmpty(restaurant1) || TextUtils.isEmpty(menu1)) {
            restaurantName.setError("Your message");
            return;
        }else{
        restaurant.setRestaurantName(restaurant1);
        restaurant.setRestaurantMenu(menu1);
        }
    }*/




    @Override
    public void onStart() {
        super.onStart();

        if (!checkPermissions()) {
            requestPermissions();
        } else {
            getLastLocation();
        }
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                getLastLocation();
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                showSnackbar(R.string.permission_denied, R.string.setting,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");

            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            startLocationPermissionRequest();
                        }
                    });

        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest();
        }
    }

    private void showSnackbar(final String text) {
        View container = findViewById(R.id.add_restaurant_layout);
        if (container != null) {
            Snackbar.make(container, text, Snackbar.LENGTH_LONG).show();
        }
    }

    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();

                            addLatitude.setText(String.valueOf(mLastLocation.getLatitude()));
                            addLongitude.setText(String.valueOf(mLastLocation.getLongitude()));

                            restaurant.setLatitude(mLastLocation.getLatitude());
                            restaurant.setLongitude(mLastLocation.getLongitude());
                        } else {
                            Log.w(TAG, "getLastLocation:exception", task.getException());
                            showSnackbar(getString(R.string.no_location_detected));
                        }
                    }
                });
    }


    public void createFANViewAndListener() {

        doneFAB = (FloatingActionButton) findViewById(com.diabetespartner.application.R.id.done_fab);
        doneFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogOnAddButtonPressed();
                //getRestaurantDetails();
                String restaurant1 = restaurantName.getText().toString();
                String menu1 = menu.getText().toString();

                if(TextUtils.isEmpty(restaurant1)) {
                    restaurantName.setError("Please Fill in, Ohh Please !");
                    return;
                }else if(TextUtils.isEmpty(menu1)) {
                    menu.setError("Please Fill in, Ohh Please !");
                    return;

                }else{
                    restaurant.setRestaurantName(restaurant1);
                    restaurant.setRestaurantMenu(menu1);

                    if(ref != null) {
                        ref.child("Restaurant").push().setValue(restaurant);
                        finish();
                        startActivity(new Intent(getApplicationContext(), MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    }
                }



            }
        });
        fabAnimationRunnable = new Runnable() {
            @Override
            public void run() {
                AnimationTools.startCircularReveal(doneFAB);
            }
        };
        doneFAB.postDelayed(fabAnimationRunnable, 600);
    }

    protected void onDestroy() {
        super.onDestroy();
        doneFAB.removeCallbacks(fabAnimationRunnable);
    }

    protected void dialogOnAddButtonPressed() {

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        finishActivity();
    }

    public void finishActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        // Pass pager position to open it again later
        Bundle b = new Bundle();
        b.putInt(INTENT_EXTRA_PAGER, this.getPagerPosition());
        b.putInt(INTENT_EXTRA_DROPDOWN, this.getDropdownPosition());
        intent.putExtras(b);
        startActivity(intent);
        finish();
    }

    public int getPagerPosition() {
        return pagerPosition;
    }
    public int getDropdownPosition() {
        return dropdownPosition;
    }
}
