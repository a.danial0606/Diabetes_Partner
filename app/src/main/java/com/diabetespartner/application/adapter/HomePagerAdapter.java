package com.diabetespartner.application.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.diabetespartner.application.fragment.HistoryFragment;
import com.diabetespartner.application.fragment.OverviewFragment;

public class HomePagerAdapter extends FragmentPagerAdapter {

    private Context mContext;
    private OverviewFragment overviewFragment;
    private HistoryFragment historyFragment;


    public HomePagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.mContext = context;
        overviewFragment = OverviewFragment.newInstance();
        historyFragment = HistoryFragment.newInstance();
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return overviewFragment;
            default:
                return historyFragment;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    // Workaround to refresh views with notifyDataSetChanged()
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(com.diabetespartner.application.R.string.tab_overview);
            default:
                return mContext.getString(com.diabetespartner.application.R.string.tab_history);
        }
    }


    public OverviewFragment getOverviewFragment() {
        return overviewFragment;
    }

    public void setOverviewFragment(OverviewFragment overviewFragment) {
        this.overviewFragment = overviewFragment;
    }

    public HistoryFragment getHistoryFragment() {
        return historyFragment;
    }

    public void setHistoryFragment(HistoryFragment historyFragment) {
        this.historyFragment = historyFragment;
    }

}