package com.diabetespartner.application.object;

/**
 * Created by ADANI on 11/15/2017.
 */

public class Restaurant {
    private String restaurantName;
    private String restaurantMenu;
    private double latitude;
    private double longitude;

    public Restaurant(){}

    public Restaurant(String restaurantName, String restaurantMenu, double latitude, double longitude) {
        this.restaurantName = restaurantName;
        this.restaurantMenu = restaurantMenu;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName){
        this.restaurantName = restaurantName;
    }

    public String getRestaurantMenu() {
        return restaurantMenu;
    }

    public void setRestaurantMenu(String restaurantMenu){
        this.restaurantMenu = restaurantMenu;
    }

    public double getLatitude(){
        return latitude;
    }

    public void setLatitude(double latitude){
        this.latitude = latitude;
    }

    public void setLongitude(double longitude){
        this.longitude  = longitude;
    }

    public double getLongitude(){
        return longitude;
    }


}
