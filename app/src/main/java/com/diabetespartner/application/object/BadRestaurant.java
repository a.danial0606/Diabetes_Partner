package com.diabetespartner.application.object;

/**
 * Created by ADANI on 11/20/2017.
 */

public class BadRestaurant {
    private double latitude;
    private double longitude;
    private String restaurantName;


    public BadRestaurant(){}

    public BadRestaurant(double latitude, double longitude, String restaurantName) {
        this.restaurantName = restaurantName;
        this.latitude = latitude;
        this.longitude = longitude;
    }
    public double getLongitude(){
        return longitude;
    }
    public double getLatitude(){
        return latitude;
    }
    public String getRestaurantName() {
        return restaurantName;
    }
}
