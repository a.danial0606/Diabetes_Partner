package com.diabetespartner.application.view;

public interface HelloView {
    void displayErrorWrongAge();

    void startMainView();
}
