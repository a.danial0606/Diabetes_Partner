package com.diabetespartner.application.receivers;

import android.content.Context;
import android.content.Intent;

import com.diabetespartner.application.tools.DiabetesPartnerAlarmManager;
import com.diabetespartner.application.tools.NotificationManager;

public class BroadcastReceiver extends android.content.BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            setAlarms(context);
        } else {
            if (intent.getBooleanExtra("glucosio_reminder", false)) {
                NotificationManager notificationManager = new NotificationManager(context);
                String reminderLabel = intent.getStringExtra("reminder_label");
                notificationManager.sendReminderNotification(reminderLabel);
            } else {
                setAlarms(context);
            }
        }
    }

    private void setAlarms(Context context) {
        DiabetesPartnerAlarmManager alarmManager = new DiabetesPartnerAlarmManager(context);
        alarmManager.setAlarms();
    }
}